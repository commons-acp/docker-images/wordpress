FROM wordpress:php7.2-apache
RUN apt update -y
RUN apt install wget zip -y
RUN wget "https://gitlab.com/commons-acp/docker-images/wordpress/-/raw/main/import_config"
COPY import_config /usr/bin/import_config
RUN chmod +x /usr/bin/import_config

